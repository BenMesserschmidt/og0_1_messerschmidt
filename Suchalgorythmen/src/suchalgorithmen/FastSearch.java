package suchalgorithmen;

public class FastSearch implements ISuchalgorithmus {

	@Override
	public int suche(long[] zahlen, long gesuchteZahl) {
		int l = 0, r = zahlen.length - 1;
		int m_b, m_i;

		while (l <= r) {
			// Bereich halbieren
			m_b = l + ((r - l) / 2);
			m_i = (l + (int)((double)(gesuchteZahl - zahlen[l]) / (zahlen[r] - zahlen[l]) * (r - l)));

			if (m_b > m_i) {
				int h = m_b;
				m_b = m_i;
				m_i = h;
			}

			if (zahlen[m_b] == gesuchteZahl) // Element gefunden?
				return m_b;
			if (zahlen[m_i] == gesuchteZahl) // Element gefunden?
				return m_i;

			if (zahlen[m_b] > gesuchteZahl)
				r = m_b - 1; // im linken Abschnitt weitersuchen
			else if (zahlen[m_i] > gesuchteZahl) {
				l = m_b + 1;// im mittleren Abschnitt weitersuchen
				r = m_i - 1;
			} else
				l = m_i + 1; // im rechten Abschnitt weitersuchen
		}
		return NICHT_GEFUNDEN;
	}

	@Override
	public int getVersuche(long[] zahlen, long gesuchteZahl) {
		int l = 0, r = zahlen.length - 1;
		int m_b, m_i;
		int vergleiche = 0;

		while (l <= r) {
			// Bereich halbieren
			m_b = l + ((r - l) / 2);
			m_i = (l + (int)((double)(gesuchteZahl - zahlen[l]) / (zahlen[r] - zahlen[l]) * (r - l)));

			if (m_b > m_i) {
				int h = m_b;
				m_b = m_i;
				m_i = h;
			}

			vergleiche++;
			if (zahlen[m_b] == gesuchteZahl) // Element gefunden?
				return vergleiche;
			vergleiche++;
			if (zahlen[m_i] == gesuchteZahl) // Element gefunden?
				return vergleiche;

			vergleiche++;
			if (zahlen[m_b] > gesuchteZahl)
				r = m_b - 1; // im linken Abschnitt weitersuchen
			else {
				vergleiche++;
				if (zahlen[m_i] > gesuchteZahl) {
					l = m_b + 1;// im mittleren Abschnitt weitersuchen
					r = m_i - 1;
				} else
					l = m_i + 1; // im rechten Abschnitt weitersuchen
			}
		}
		return vergleiche;
	}

}
