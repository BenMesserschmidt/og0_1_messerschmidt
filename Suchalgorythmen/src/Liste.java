import listengenerator.SortedListGenerator;
import suchalgorithmen.LineareSuche;

public class Liste {
	public static void main(String[] args) {

		long[] zahlen;
		zahlen = SortedListGenerator.getSortedList(100);

		LineareSuche suchalgorithmus = new LineareSuche();

		System.out.println(suchalgorithmus.suche(zahlen, 50));
		System.out.println(suchalgorithmus.getVersuche(zahlen, 9));

	}
}
